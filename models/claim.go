package models

import jwt "github.com/dgrijalva/jwt-go"

// JWTClaim struct
type JWTClaim struct {
	Username string `json:"username"`

	jwt.StandardClaims
}
