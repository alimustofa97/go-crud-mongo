package models

import (
	"fmt"

	util "gitlab.com/alimustofa/go-crud-mongo/utils"
	"gopkg.in/mgo.v2/bson"
)

// User struct
type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// GetUser func
func (u User) GetUser() (interface{}, error) {
	var session, db, err = util.Connect()

	if err != nil {
		panic(err)
	}
	defer session.Close()

	fmt.Println(u.Username, u.Password)
	var user User
	errQuery := db.C("users").Find(bson.M{"username": u.Username, "password": u.Password}).One(&user)

	if errQuery != nil {
		return nil, errQuery
	}

	return user, nil
}
