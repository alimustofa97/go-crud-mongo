package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"gitlab.com/alimustofa/go-crud-mongo/models"
)

// ResponseJSON struct
type ResponseJSON struct {
	Error   bool        `json:"error"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

var Person = models.Person{}

func main() {
	router := NewRouter()

	log.Print("Server listening on port 8000")
	errServer := http.ListenAndServe(":8000", router)

	if errServer != nil {
		log.Fatal("Server error", errServer)
	}
}

/*SendResponseJSON function*/
func SendResponseJSON(w http.ResponseWriter, error bool, data interface{}, message string) {
	w.Header().Set("Content-Type", "application/json;charset=UTF-8")

	response := ResponseJSON{
		Error:   error,
		Message: message,
		Data:    data,
	}

	if err := json.NewEncoder(w).Encode(&response); err != nil {
		panic(err)
	}
}

/*GetPeople function*/
func GetPeople(w http.ResponseWriter, r *http.Request) {
	result, err := Person.All()
	if err != nil {
		panic(err)
	}

	SendResponseJSON(w, false, result, "Data people")
}

/*GetPerson function*/
func GetPerson(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]

	idInt, _ := strconv.Atoi(id)
	result, err := Person.Get(idInt)

	if err != nil && err.Error() == "not found" {
		SendResponseJSON(w, true, nil, "Data not found")
	} else if err != nil {
		panic(err)
	} else {
		SendResponseJSON(w, false, result, "Data person")
	}

}

/*CreatePerson function*/
func CreatePerson(w http.ResponseWriter, r *http.Request) {
	var person models.Person
	json.NewDecoder(r.Body).Decode(&person)

	result, err := Person.Create(person)

	if err != nil {
		panic(err)
	}

	SendResponseJSON(w, false, result, "Data person")
}

/*UpdatePerson function*/
func UpdatePerson(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	idInt, _ := strconv.Atoi(params["id"])

	_, errGet := Person.Get(idInt)

	if errGet != nil && errGet.Error() == "not found" {
		SendResponseJSON(w, true, nil, "Data not found")
	} else if errGet != nil {
		panic(errGet)
	} else {
		var changes interface{}
		json.NewDecoder(r.Body).Decode(&changes)

		result, err := Person.Update(idInt, changes)

		if err != nil {
			panic(err)
		}

		SendResponseJSON(w, false, result, "Success update")
	}

}

/*DeletePerson function*/
func DeletePerson(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	idInt, _ := strconv.Atoi(params["id"])

	_, errGet := Person.Get(idInt)

	if errGet != nil && errGet.Error() == "not found" {
		SendResponseJSON(w, true, nil, "Data not found")
	} else if errGet != nil {
		panic(errGet)
	} else {
		_, errDel := Person.Delete(idInt)

		if errDel != nil {
			panic(errDel)
		}

		SendResponseJSON(w, false, nil, "Success delete")
	}
}

/*Login function*/
func Login(w http.ResponseWriter, r *http.Request) {
	var data map[string]string
	json.NewDecoder(r.Body).Decode(&data)

	var userReq = models.User{Username: data["username"], Password: data["password"]}
	_, errUser := userReq.GetUser()

	if errUser != nil {
		SendResponseJSON(w, true, nil, "Login failed")
	} else {

		claimData := models.JWTClaim{Username: data["username"]}
		token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), claimData)

		tokenstring, err := token.SignedString([]byte("apiKey"))
		if err != nil {
			log.Fatalln(err)
		}

		SendResponseJSON(w, false, tokenstring, "Login success")
	}
}

/*Me function*/
func Me(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var tokenData = params["token"]

	// In another way, you can decode token to your struct, which needs to satisfy `jwt.StandardClaims`
	claimData := models.JWTClaim{}
	token, _ := jwt.ParseWithClaims(tokenData, &claimData, func(token *jwt.Token) (interface{}, error) {
		return []byte("apiKey"), nil
	})

	if !token.Valid {
		SendResponseJSON(w, true, nil, "Data invalid")
	} else {
		SendResponseJSON(w, false, token.Claims.(*models.JWTClaim).Username, "Profile")
	}

}

// IsAlive func
func IsAlive(w http.ResponseWriter, r *http.Request) {
	SendResponseJSON(w, false, nil, "Alive")
}
