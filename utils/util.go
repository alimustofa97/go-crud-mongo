package utils

import (
	"os"

	mgo "gopkg.in/mgo.v2"
)

// Connect func
func Connect() (*mgo.Session, *mgo.Database, error) {
	host := os.Getenv("DB_HOST")
	if host == "" {
		host = "localhost:27017"
	}

	var session, err = mgo.Dial(host)
	if err != nil {
		return nil, nil, err
	}

	session.SetMode(mgo.Monotonic, true)

	db := session.DB("people_db")
	return session, db, nil
}
