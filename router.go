package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

// AuthMiddleware func
func AuthMiddleware(f http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// TODO: check account from header

		isError := true

		if isError {
			SendResponseJSON(w, true, nil, "Unauthorize")
			return
		}

		f(w, r)
	}
}

// NewRouter asdfasdf
func NewRouter() *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/is_alive", AuthMiddleware(IsAlive)).Methods("GET")

	router.HandleFunc("/people", GetPeople).Methods("GET")
	router.HandleFunc("/people", CreatePerson).Methods("POST")
	router.HandleFunc("/people/{id}", GetPerson).Methods("GET")
	router.HandleFunc("/people/{id}", UpdatePerson).Methods("PUT")
	router.HandleFunc("/people/{id}", DeletePerson).Methods("DELETE")

	router.HandleFunc("/login", Login).Methods("POST")
	router.HandleFunc("/me/{token}", Me).Methods("GET")

	return router
}
