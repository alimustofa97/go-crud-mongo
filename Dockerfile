FROM golang

RUN go get github.com/golang/dep/cmd/dep
RUN go get github.com/pilu/fresh

ENV SRC_DIR=/go/src/gitlab.com/alimustofa/go-crud-mongo
COPY Gopkg.lock Gopkg.toml $SRC_DIR/
WORKDIR $SRC_DIR

COPY . $SRC_DIR
RUN dep ensure

CMD [ "fresh" ]